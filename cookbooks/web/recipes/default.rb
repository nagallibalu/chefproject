#
# Cookbook Name:: web
# Recipe:: default
#
# Copyright 2017, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

service 'apache' do
  action [ :enable, :start ]
  retries 3
end